/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ControleCliente;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author vinicius
 */
public class ControleCliente {
    
    private ArrayList<Cliente> lista;

    public ControleCliente() {
        carregaArquivo();
    }
    
    public void cadastraCliente(Cliente c){
        lista.add(c);
        gravaArquivo();
    }
    
    public void removeCliente(int k){
        lista.remove(k);
        gravaArquivo();
    }
    
    public ArrayList<Cliente> getListaClientes(){
        return lista;
    }

    public void carregaArquivo(){
        try {
            InputStream in = new FileInputStream("clientes.clt");
            ObjectInput obj = new ObjectInputStream(in);
            lista = (ArrayList<Cliente>) obj.readObject();
        } catch (FileNotFoundException ex) {
            lista = new ArrayList<Cliente>();
            gravaArquivo();
        } catch (IOException ex) {
            Logger.getLogger(ControleCliente.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ControleCliente.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    public void gravaArquivo(){
        try {
            OutputStream out = new FileOutputStream("clientes.clt");
            ObjectOutputStream obj = new ObjectOutputStream(out);
            obj.writeObject(lista);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ControleCliente.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ControleCliente.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
